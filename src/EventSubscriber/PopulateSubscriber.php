<?php

namespace App\EventSubscriber;

use App\Event\Populate;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PopulateSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
           Populate::DATE_EVENT => array('addDate')
        );
    }

    public function addDate(Populate $event)
    {
        $instance = $event->getInstance();
        !$instance->getCreatedAt() ?
                $instance->setCreatedAt(new \DateTime()) : null;
    }
}
