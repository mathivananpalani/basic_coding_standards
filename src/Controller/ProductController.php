<?php

namespace App\Controller;

use DateTime;
use App\Entity\Product;
use App\Form\ProductType;
use App\Service\Product as ServiceProduct;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Product Controller
 */
class ProductController extends AbstractController
{
    private $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    public function add(Request $request, ServiceProduct $productService)
    {
        $productName = $request->get('productName', null);
        $em = $this->getDoctrine()->getManager();
        if ($productName) {
            $product = $productService->findProduct($productName);
        } else {
            $product = new Product();
            $product->setCreatedAt(new DateTime());
        }
        $form = $this->createForm(ProductType::class, $product);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();
            $em->persist($product);
            $em->flush();
            $em->clear();

            return $this->redirectToRoute('add_product');
        }

        $products = $productService->getProducts();

        return $this->render('product/product.html.twig', array(
            'base_dir' => realpath($this->parameterBag->get('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'products' => $products,
            'edit' => isset($productName),
            'event' => $request->get('event'),
            'count' => count($products)
        ));
    }

    public function delete($productName, ServiceProduct $productService)
    {
        $productService->remove($productName);
        return $this->redirectToRoute('add_product');
    }
}
