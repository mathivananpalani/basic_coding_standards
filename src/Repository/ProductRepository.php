<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findAllAndOrderByDate()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.createdAt', 'asc')
            ->getQuery()
            ->getResult();
    }

    public function removeByName($productName)
    {
        $this->createQueryBuilder('p')
            ->delete()
            ->where('p.product = :product')
            ->setParameter(':product', $productName)
            ->getQuery()
            ->execute();
    }

    public function findByName($productName)
    {
        return $this->findOneBy(['product' => $productName]);
    }
}
