<?php

namespace App\Service;

use App\Entity\Product as EntityProduct;
use Doctrine\ORM\EntityManagerInterface;

class Product
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getProducts()
    {
        $products = $this->em->getRepository(EntityProduct::class)
            ->findAllAndOrderByDate();

        return $this->formatProducts($products);
    }

    public function findProduct($productName)
    {
        $product = $this->em->getRepository(EntityProduct::class)
            ->findByName($productName);
        return $product;
    }

    public function remove($productName)
    {
        $this->em->getRepository(EntityProduct::class)
            ->removeByName($productName);
    }

    private function formatProducts(array $products)
    {
        $formated = [];

        foreach ($products as $key => $value) {
            array_push($formated, [
                'name' => $value->getProduct(),
                'description' => $value->getDescription(),
                'price' => $value->getPrice(),
                'date' => $value->getCreatedAt()
            ]);
        }
        return $formated;
    }
}
