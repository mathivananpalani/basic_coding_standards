<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DateConstraint extends Constraint
{
    public $message = 'Date should be current date or less';

    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }
}
