<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

class Populate extends Event
{
    public const DATE_EVENT = 'populate.date_event';
    private $instance;

    public function __construct($obj)
    {
        $this->instance = $obj;
    }

    public function getInstance()
    {
        return $this->instance;
    }
}
