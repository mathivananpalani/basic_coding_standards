<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthControllerTest extends WebTestCase
{
    private $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testLoginPage()
    {
        $crawler = $this->client->request('GET', '/login');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsString('Login', $crawler->filter('h2')->text());
        $this->client->restart();
    }

    // public function testLoginForm()
    // {
    //     $crawler = $this->client->request('GET', '/login');
    //     $submitButton = $crawler->filter('[type=submit]');
    //     $form = $submitButton->form();
    //     $form['_username'] = 'admin@gmail.com';
    //     $form['_password'] = 'password';
    //     $form['_target_path'] = '/product';
    //     $loginCrawler = $this->client->submit($form);
    //     $this->assertResponseRedirects();
    //     $this->assertStringContainsString('Add Products', $loginCrawler->filter('h1')->text());
    // }
}
